import {
    USERS_FETCH_ERROR,
    USERS_FETCH_PENDING,
    USERS_FETCH_SUCCESS,
    USERS_PAGE_CHANGE,
    USERS_CLICK_EDIT_USER, 
    CLOSE_MODAL_EDIT_USER,
    CHANGE_ROW_NUMBER,
    USER_CLICK_ADD_USER, 
    ADD_USER_FIRST_NAME,
    ADD_USER_LAST_NAME,
    ADD_USER_SUBJECT,
    ADD_USER_COUNTRY,
    ADD_USER_CUSTOMER_TYPE,
    USER_CLICK_ADD_USER_PENDING, 
    USER_CLICK_ADD_USER_SUCCESS,
    USER_CLICK_ADD_USER_ERROR, 
    CLOSE_MODAL_ADD_USER,
    EDIT_USER_FIRST_NAME,
    EDIT_USER_LAST_NAME,
    EDIT_USER_SUBJECT,
    EDIT_USER_COUNTRY,
    EDIT_USER_CUSTOMER_TYPE,
    EDIT_USER_REGISTER_STATUS,
    EDIT_USER_PENDING,
    EDIT_USER_SUCCESS,
    EDIT_USER_ERROR,
    OPEN_DELETE_USER_MODAL,
    CLOSE_MODAL_DELETE_USER,
    DELETE_USER_PENDING,
    DELETE_USER_SUCCESS,
    DELETE_USER_ERROR
} from "../constants/users.constants";
import axios from "axios";

export const fetchUsers = (page, limit) => {
    return async (dispatch) => {
        try {
            var requestOptions = {
                method: 'GET',
                redirect: 'follow'
            };
    
            await dispatch({
                type: USERS_FETCH_PENDING
            })

            const responseTotalUser = await fetch("http://203.171.20.210:8080/devcamp-register-java-api/users", requestOptions);

            const dataTotalUser = await responseTotalUser.json();
            // console.log( dataTotalUser)
            
            const params = new URLSearchParams({
                page: page - 1, 
                size: limit
            });
        
            const response = await fetch("http://203.171.20.210:8080/devcamp-register-java-api/user-list-pagination?" + params.toString(), requestOptions);

            const data = await response.json();
            
            return dispatch({
                type: USERS_FETCH_SUCCESS,
                totalUser: dataTotalUser.length,
                data: data
            })
        } catch (error) {
            return dispatch({
                type: USERS_FETCH_ERROR,
                error: error
            })
        }   
    }
}

export const pageChangePagination = (page) => {
    return {
        type: USERS_PAGE_CHANGE,
        page: page
    }
}

export const showModalEditUser = (user) => {
    return {
        type: USERS_CLICK_EDIT_USER,
        page: user
    }
}
export const closeModalEditUser = () => {
    return{
        type: CLOSE_MODAL_EDIT_USER
    }
}

export const changeRowNumber = (Rows) => {
    return{
        type: CHANGE_ROW_NUMBER,
        page: Rows
    }
}

export const showModalAddUser = () => {
     return{
        type: USER_CLICK_ADD_USER
     }
}

//thêm thông tin user trên modal add user
export const onChangeFirstNameAddUser = (value) => {
    return{
        type: ADD_USER_FIRST_NAME,
        page: value
    }
}

export const onChangeLastNameAddUser = (value) => {
    return{
        type: ADD_USER_LAST_NAME,
        page: value
    }
}
export const onChangeSubjectAddUser = (value) => {
    return{
        type: ADD_USER_SUBJECT,
        page: value
    }
}

export const onChangeCountryAddUser = (value) => {
    return{
        type: ADD_USER_COUNTRY,
        page: value
    }
}

export const onChangeCustomerTypeAddUser = (value) => {
    return{
        type: ADD_USER_CUSTOMER_TYPE,
        page: value
    }
}
export const addUserOnModal = (value) => {
    return async (dispatch) => {
        dispatch({ type: USER_CLICK_ADD_USER_PENDING});
        axios({
            method: "post",
            url: `http://203.171.20.210:8080/crud-api/users/`,
            data: JSON.stringify(value),
            headers: {
                "Content-Type": "application/json",
            },
        })
            .then(data => {
                dispatch({type: USER_CLICK_ADD_USER_SUCCESS, page: data});
            })
            .catch(error => {
                dispatch({ type: USER_CLICK_ADD_USER_ERROR, error});
                console.log(error.response);
        })
    }
}
export const closeModalAddUser = (value) => {
    return {
        type: CLOSE_MODAL_ADD_USER
    }
}

//MODAL EDIT USER
export const onChangeFirstNameEditUser = (value) => {
    return{
        type: EDIT_USER_FIRST_NAME,
        page: value
    }
}
export const onChangeLastNameEditUser = (value) => {
    return{
        type: EDIT_USER_LAST_NAME,
        page: value
    }
}
export const onChangeSubjectEditUser = (value) => {
    return{
        type: EDIT_USER_SUBJECT,
        page: value
    }
}
export const onChangeCountryEditUser = (value) => {
    return{
        type: EDIT_USER_COUNTRY,
        page: value
    }
}
export const onChangeCustomerTypeEditUser = (value) => {
    return{
        type: EDIT_USER_CUSTOMER_TYPE,
        page: value
    }
}
export const onChangeRegisterStatusEditUser = (value) => {
    return{
        type: EDIT_USER_REGISTER_STATUS,
        page: value
    }
}
export const editUserOnModal = (value, id) => {
    return async (dispatch) => {
        dispatch({ type : EDIT_USER_PENDING});
        axios({
            method: "put",
            url: `http://203.171.20.210:8080/crud-api/users/`+ id,
            data: JSON.stringify(value),
            headers: {
                "Content-Type": "application/json",
            }
        })
        .then(data => {
            dispatch({ type: EDIT_USER_SUCCESS, page: data.data})
        })
        .catch(error => {
            dispatch({ type: EDIT_USER_ERROR, error});
            console.log(error.response);
        })
    }
}
export const openDeleteUserModal = (value) => {
    return{
        type: OPEN_DELETE_USER_MODAL,
        page: value,
    }
}
export const closeModalDeleteUser = () => {
    return{
        type: CLOSE_MODAL_DELETE_USER
    }
}
export const delteUserOnModal = (id) => {
    return async (dispatch) => {
        dispatch({ type : DELETE_USER_PENDING});
        axios({
            method: "delete",
            url: `http://203.171.20.210:8080/crud-api/users/`+ id,
        })
        .then(data => {
            dispatch({ type: DELETE_USER_SUCCESS, page: data.data})
        })
        .catch(error => {
            dispatch({ type: DELETE_USER_ERROR, error});
            console.log(error.response);
        })
    }
}