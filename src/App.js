
import './App.css';

import Users from "./components/users.component";

function App() {
  return (
    <div>
      <h1 align="center" >Danh sách đăng ký</h1>
      <Users />
    </div>
  );
}

export default App;
