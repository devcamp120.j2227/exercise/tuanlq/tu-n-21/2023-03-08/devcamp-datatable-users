import {
    USERS_FETCH_ERROR,
    USERS_FETCH_PENDING,
    USERS_FETCH_SUCCESS,
    USERS_PAGE_CHANGE,
    USERS_CLICK_EDIT_USER, 
    CLOSE_MODAL_EDIT_USER, 
    CHANGE_ROW_NUMBER, 
    USER_CLICK_ADD_USER,
    ADD_USER_FIRST_NAME,
    ADD_USER_LAST_NAME,
    ADD_USER_SUBJECT,
    ADD_USER_COUNTRY,
    ADD_USER_CUSTOMER_TYPE,  
    USER_CLICK_ADD_USER_PENDING, 
    USER_CLICK_ADD_USER_SUCCESS,
    USER_CLICK_ADD_USER_ERROR,
    CLOSE_MODAL_ADD_USER,
    EDIT_USER_FIRST_NAME,
    EDIT_USER_LAST_NAME,
    EDIT_USER_COUNTRY,
    EDIT_USER_SUBJECT,
    EDIT_USER_CUSTOMER_TYPE,
    EDIT_USER_REGISTER_STATUS,
    EDIT_USER_PENDING,
    EDIT_USER_SUCCESS,
    EDIT_USER_ERROR,
    OPEN_DELETE_USER_MODAL,
    CLOSE_MODAL_DELETE_USER,
    DELETE_USER_PENDING,
    DELETE_USER_SUCCESS,
    DELETE_USER_ERROR
} from "../constants/users.constants";

const initialState = {
    users: [],
    limit: 10,
    pending: false,
    error: null,
    noPage: 0,
    currentPage: 1,
    modalEditUser: false,
    userDetail:{
        
    },
    userAdded: {
        firstname: '',
        lastname: "",
        subject: "",
        country: "",
        customerType: ""
    },
    modalAddUser: false,
    userEdited: {
        firstname: '',
        lastname: "",
        subject: "",
        country: "",
        customerType: "",
        registerStatus:""
    },
    modalDeleteUser: false,
}

const userReducers = (state = initialState, action) => {
    switch (action.type) {
        case USERS_FETCH_PENDING:
            state.pending = true;
            break;
        case USERS_FETCH_SUCCESS:
            state.pending = false;
            state.noPage = Math.ceil(action.totalUser / state.limit);
            state.users = action.data;
            break;
        case USERS_FETCH_ERROR:
            break;
        case USERS_PAGE_CHANGE:
            state.currentPage = action.page;
            break;
        //Edit user
        case USERS_CLICK_EDIT_USER:
            state.userDetail = action.page;
            console.log(state.userDetail)
            state.modalEditUser = true
            // state.modalEditUser = true;
            break;
        case EDIT_USER_FIRST_NAME:
            state.userEdited.firstname = action.page;
            break;
        case EDIT_USER_LAST_NAME:
            state.userEdited.lastname = action.page;
            break;
        case EDIT_USER_SUBJECT:
            state.userEdited.subject = action.page;
            break;
        case EDIT_USER_COUNTRY:
            state.userEdited.country = action.page;
            break;
        case EDIT_USER_CUSTOMER_TYPE:
            state.userEdited.customerType = action.page;
            break;
        case EDIT_USER_REGISTER_STATUS :
            state.userEdited.registerStatus = action.page;
            console.log(state.userEdited)
            break;
        case CLOSE_MODAL_EDIT_USER:
            state.modalEditUser = false;
            break;
        case EDIT_USER_PENDING:
            break;
        case EDIT_USER_SUCCESS:
            alert("Edit User thành công!");
            console.log(action.page);
            state.modalEditUser = false;
            break;
        case EDIT_USER_ERROR:
            alert("Xảy ra lỗi khi edit user!");
            break;
        //change table row number
        case CHANGE_ROW_NUMBER:
            state.limit = action.page;
            break;
        //Add user
        case USER_CLICK_ADD_USER:
            state.modalAddUser = true;
            break;
        case ADD_USER_FIRST_NAME:
            state.userAdded.firstname = action.page;
            break;
        case ADD_USER_LAST_NAME:
            state.userAdded.lastname = action.page;
            break;
        case ADD_USER_SUBJECT:
            state.userAdded.subject = action.page;
            break;
        case ADD_USER_COUNTRY: 
            state.userAdded.country = action.page;
            break;
        case ADD_USER_CUSTOMER_TYPE:
            state.userAdded.customerType = action.page;
            break;
        case USER_CLICK_ADD_USER_PENDING:
            break;
        case USER_CLICK_ADD_USER_SUCCESS:
            state.modalAddUser = false;
            console.log(action.page.data);
            alert("Add User thành công!");
            state.modalAddUser = false;
            break;
        case USER_CLICK_ADD_USER_ERROR:
            alert("Xảy ra lỗi khi add user!");
            break;
        case CLOSE_MODAL_ADD_USER:
            state.modalAddUser = false;
            break;
        //MODAL DELETE USER
        case OPEN_DELETE_USER_MODAL:
            state.userDetail = action.page;
            state.modalDeleteUser = true;
            break;
        case CLOSE_MODAL_DELETE_USER:
            state.modalDeleteUser = false;
            break;
        case DELETE_USER_PENDING:
            break;
        case DELETE_USER_SUCCESS:
            alert("Delete user thành công!")
            state.modalDeleteUser = false;
            break;
        case DELETE_USER_ERROR:
            alert("Xảy ra lỗi khi delete user!");
            break;
        default:
            break;
    }

    return {...state};
}

export default userReducers;