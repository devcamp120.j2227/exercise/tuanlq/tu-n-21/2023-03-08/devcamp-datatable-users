import { Button, Grid } from "@mui/material"
import { useDispatch } from "react-redux";
import { useSelector } from "react-redux";
import {showModalAddUser} from "../../actions/users.actions";
export default function ButtonAddUser(){
    const dispatch = useDispatch();
    const {  modalAddUser} = useSelector((reduxData) => reduxData.usersReducers);
    const handlerAddUserClick = () => {
        dispatch(showModalAddUser());
    }
    return (
        <Grid item lg={6} md={6} sm={6} xs={6} mb={3}>
            <Button onClick={handlerAddUserClick}  variant='contained' color="success">Thêm user</Button>
        </Grid>
    )
}