import Modal from '@mui/material/Modal';
import Typography from '@mui/material/Typography';
import Box from '@mui/material/Box';
import { useDispatch, useSelector } from "react-redux";
import { Grid ,TextField, MenuItem, FormControl, Select, Button, InputLabel} from '@mui/material';
import { useState } from 'react';
import {closeModalAddUser, onChangeFirstNameAddUser, onChangeLastNameAddUser, onChangeSubjectAddUser, onChangeCountryAddUser, onChangeCustomerTypeAddUser, addUserOnModal } from '../../actions/users.actions';
export default function ModalAddUser (){
  const [country, setCountry] = useState("");
  const [customerType, setCustomerType] = useState("");
  const dispatch = useDispatch();
  const { userAdded, modalAddUser} = useSelector((reduxData) => reduxData.usersReducers);
    const style = {
        position: 'absolute',
        top: '50%',
        left: '50%',
        transform: 'translate(-50%, -50%)',
        width: 400,
        bgcolor: 'background.paper',
        boxShadow: 24,
        p: 4,
        borderRadius: "10px"
      };
    //thêm giá trị user vào store
    const onChangeFirstName = (event) => {
        dispatch(onChangeFirstNameAddUser(event.target.value));
    }
    const onChangeLastName = (event) => {
        dispatch(onChangeLastNameAddUser(event.target.value));
    }
    const onChangeSubject = (event) => {
      dispatch(onChangeSubjectAddUser(event.target.value));
    }
    const onChangeCountry= (event) => {
      setCountry(event.target.value);
      dispatch(onChangeCountryAddUser(event.target.value));
    }
    const onChangeCustomerType= (event) => {
      setCustomerType(event.target.value);
      dispatch(onChangeCustomerTypeAddUser(event.target.value));
    }
    const handlerCloseModal = () => {
      dispatch(closeModalAddUser());
    }
    const handlerAddUser = () => {
      if(userAdded.firstname === ""){
        alert("Bạn chưa điền first name");
        return false
      }
      if(userAdded.lastname === ""){
          alert("Bạn chưa điền last name");
          return false
      }
      if(userAdded.country === ""){
          alert("Bạn chưa điền country");
          return false
      }
      if(userAdded.customerType === ""){
          alert("Bạn chưa điền customer type");
          return false
      }
        console.log(userAdded);
        dispatch(addUserOnModal(userAdded));
    }
    return (
      <div>
        <Modal
          keepMounted
          open = {modalAddUser}
          onClose = {handlerCloseModal}
          aria-labelledby="keep-mounted-modal-title"
          aria-describedby="keep-mounted-modal-description"
        >
          <Box sx={style}>
                <Grid container>
                    <Grid item lg={12} md={12} sm={12} xs={12} mt={3}>
                        <Typography variant="h4">Thêm User</Typography>
                    </Grid>
                    <Grid item lg={4} md={4} sm={4} xs={4} mt={3}>
                        <Typography>First name: </Typography>
                    </Grid>
                    <Grid item lg={8} md={8} sm={8} xs={8} mt={3} sx={{width: "100%"}}>
                        <TextField  onChange={onChangeFirstName}  sx={{width: "100%"}} id="firstname" label="firstname" variant="outlined" />
                    </Grid>
                    <Grid item lg={4} md={4} sm={4} xs={4} mt={3}>
                        <Typography>Last name: </Typography> 
                    </Grid>
                    <Grid item lg={8} md={8} sm={8} xs={8} mt={3}>
                        <TextField onChange={onChangeLastName} id="lastname" label="lastname" variant="outlined" sx={{width: "100%"}}/>
                    </Grid>
                    <Grid item lg={4} md={4} sm={4} xs={4} mt={3}>
                        <Typography>Subject: </Typography>
                    </Grid>
                    <Grid item lg={8} md={8} sm={8} xs={8} mt={3}>
                        <TextField onChange={onChangeSubject} id="subject" label="subject" variant="outlined" sx={{width: "100%"}}/>
                    </Grid>
                    <Grid item lg={4} md={4} sm={4} xs={4} mt={3}>
                        <Typography>Country: </Typography>
                    </Grid>
                    <Grid item lg={8} md={8} sm={8} xs={8} mt={3}>
                        <FormControl sx={{width: "100%"}} size="medium">
                          <InputLabel >Country</InputLabel>
                            <Select value={country} 
                              label="Country"
                              onChange={onChangeCountry}>
                                <MenuItem value="VN">Việt Nam</MenuItem>
                                <MenuItem value="USA">USA</MenuItem>
                                <MenuItem value="AUS">Australia</MenuItem>
                                <MenuItem value="CAN">Canada</MenuItem>
                            </Select>
                        </FormControl>
                    </Grid>
                    <Grid item lg={4} md={4} sm={4} xs={4} mt={3}>
                        <Typography>Customer Type: </Typography>
                    </Grid>
                    <Grid item lg={8} md={8} sm={8} xs={8} mt={3}>
                        <FormControl sx={{width: "100%"}} size="medium">
                          <InputLabel >Customer type</InputLabel>
                            <Select value={customerType} 
                              label="Customer Type"
                              onChange={onChangeCustomerType}>
                                <MenuItem value="Accepted">Accepted</MenuItem>
                                <MenuItem value="Denied">Denied</MenuItem>
                                <MenuItem value="Standard">Standard</MenuItem>
                            </Select>
                        </FormControl>
                    </Grid>
                    <Grid item lg={12} md={12} sm={12} xs={12} mt={3}>
                        <Button   variant='contained' color="success" onClick={handlerAddUser}>Thêm user</Button>
                        &ensp;<Button   variant='contained' color="success" onClick={handlerCloseModal}>Huỷ bỏ</Button>
                    </Grid>
                </Grid>
          </Box> 
        </Modal>
      </div>
    );
}