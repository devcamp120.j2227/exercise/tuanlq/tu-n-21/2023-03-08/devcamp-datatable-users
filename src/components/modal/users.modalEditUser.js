import Modal from '@mui/material/Modal';
import Typography from '@mui/material/Typography';
import Box from '@mui/material/Box';
import { Grid ,TextField, MenuItem, FormControl, Select, Button, InputLabel, } from '@mui/material';
import { useDispatch, useSelector } from "react-redux";
import { useState } from 'react';
import {editUserOnModal, closeModalEditUser, onChangeFirstNameEditUser, onChangeLastNameEditUser, onChangeSubjectEditUser, onChangeCountryEditUser, onChangeCustomerTypeEditUser, onChangeRegisterStatusEditUser } from '../../actions/users.actions';


export default function ModalEditUser (){
  const { userEdited, userDetail, modalEditUser} = useSelector((reduxData) => reduxData.usersReducers);

  const [firstname,setFirstName] = useState("");
  const [lastname, setLastName] = useState("");
  const [subject, setSubject] = useState("");
  const [country, setCountry] = useState("none");
  const [customerType, setCustomerType] = useState("none");
  const [registerStatus, setRegisterStatus] = useState('none');
  
  const dispatch = useDispatch();
    const style = {
        position: 'absolute',
        top: '50%',
        left: '50%',
        transform: 'translate(-50%, -50%)',
        width: 400,
        bgcolor: 'background.paper',
        boxShadow: 24,
        p: 4,
        borderRadius: "10px"
      };
    //thêm giá trị user vào store
    const onChangeFirstName = (event) => {
        setFirstName(event.target.value);
        dispatch(onChangeFirstNameEditUser(event.target.value));
    }
    const onChangeLastName = (event) => {
        setLastName(event.target.value);
        dispatch(onChangeLastNameEditUser(event.target.value));
    }
    const onChangeSubject = (event) => {
        setSubject(event.target.value);
        dispatch(onChangeSubjectEditUser(event.target.value));
    }
    const onChangeCountry= (event) => {
        setCountry(event.target.value);
        dispatch(onChangeCountryEditUser(event.target.value));
    }
    const onChangeCustomerType = (event) => {
        setCustomerType(event.target.value);
        dispatch(onChangeCustomerTypeEditUser(event.target.value));
    }
    const onChangeRegisterStatus = (event) => {
        setRegisterStatus(event.target.value);
        dispatch(onChangeRegisterStatusEditUser(event.target.value));
    }
    const handlerCloseModal = () => {
        dispatch(closeModalEditUser());
    }
    const handlerEditUser = () => {
        if(userEdited.firstname === ""){
            alert("Bạn chưa điền first name");
            return false
        }
        if(userEdited.lastname === ""){
            alert("Bạn chưa điền last name");
            return false
        }
        if(userEdited.country === ""){
            alert("Bạn chưa điền country");
            return false
        }
        if(userEdited.customerType === ""){
            alert("Bạn chưa điền customer type");
            return false
        }
        if(userEdited.registerStatus === ""){
            alert("Bạn chưa điền register status");
            return false
        }
        dispatch(editUserOnModal(userEdited, userDetail.id));
    }
      return (
      <div>
        <Modal
          keepMounted
          open = {modalEditUser}
          onClose = {handlerCloseModal}
          aria-labelledby="keep-mounted-modal-title"
          aria-describedby="keep-mounted-modal-description"
        >
          <Box sx={style}>
            <Grid container>
                <Grid item lg={12} md={12} sm={12} xs={12} mt={3}>
                    <Typography variant="h4">Edit User</Typography>
                </Grid>
                <Grid item lg={4} md={4} sm={4} xs={4} mt={3}>
                    <Typography >First name: </Typography>
                </Grid>
                <Grid item lg={8} md={8} sm={8} xs={8} mt={3} sx={{width: "100%"}}>
                    <TextField value={firstname} onChange={onChangeFirstName}  sx={{width: "100%"}} id="firstname" label="firstname" variant="outlined" />
                </Grid>
                <Grid item lg={4} md={4} sm={4} xs={4} mt={3}>
                    <Typography>Last name: </Typography> 
                </Grid>
                <Grid item lg={8} md={8} sm={8} xs={8} mt={3}>
                    <TextField value={lastname} onChange={onChangeLastName} id="lastname" label="lastname" variant="outlined" sx={{width: "100%"}}/>
                </Grid>
                <Grid item lg={4} md={4} sm={4} xs={4} mt={3}>
                    <Typography>Subject: </Typography>
                </Grid>
                <Grid item lg={8} md={8} sm={8} xs={8} mt={3}>
                    <TextField value={subject} onChange={onChangeSubject} id="subject" label="subject" variant="outlined" sx={{width: "100%"}}/>
                </Grid>
                <Grid item lg={4} md={4} sm={4} xs={4} mt={3}>
                    <Typography>Country: </Typography>
                </Grid>
                <Grid item lg={8} md={8} sm={8} xs={8} mt={3}>
                    <FormControl sx={{width: "100%"}} size="medium">
                      <InputLabel >Country</InputLabel>
                        <Select value={country} 
                          label="Country"
                          onChange={onChangeCountry}
                          > 
                            <MenuItem value="none">None</MenuItem>
                            <MenuItem value="VN">Việt Nam</MenuItem>
                            <MenuItem value="USA">USA</MenuItem>
                            <MenuItem value="AUS">Australia</MenuItem>
                            <MenuItem value="CAN">Canada</MenuItem>
                        </Select>
                    </FormControl>
                </Grid>
                <Grid item lg={4} md={4} sm={4} xs={4} mt={3}>
                    <Typography>Customer Type: </Typography>
                </Grid>
                <Grid item lg={8} md={8} sm={8} xs={8} mt={3}>
                    <FormControl sx={{width: "100%"}} size="medium">
                      <InputLabel >Customer type</InputLabel>
                        <Select value={customerType} 
                          label="Customer Type"
                          onChange={onChangeCustomerType}>
                            <MenuItem value="none">None</MenuItem>
                            <MenuItem value="Standard">Standard</MenuItem>
                            <MenuItem value="Silver" disabled>Silver</MenuItem>
                            <MenuItem value="Gold">Gold</MenuItem>
                            <MenuItem value="Premium">Premium</MenuItem>
                        </Select>
                    </FormControl>
                </Grid>
                <Grid item lg={4} md={4} sm={4} xs={4} mt={3}>
                    <Typography>Register Status: </Typography>
                </Grid>
                <Grid item lg={8} md={8} sm={8} xs={8} mt={3}>
                    <FormControl sx={{width: "100%"}} size="medium">
                      <InputLabel >Register Status</InputLabel>
                        <Select value={registerStatus} 
                          label="Register Status"
                          onChange={onChangeRegisterStatus}>
                            <MenuItem value="none">None</MenuItem>
                            <MenuItem value="Accepted">Accepted</MenuItem>
                            <MenuItem value="Denied">Denied</MenuItem>
                            <MenuItem value="Standard">Standard</MenuItem>
                            <MenuItem value="New" disabled>New</MenuItem>
                        </Select>
                    </FormControl>
                </Grid>
                <Grid item lg={12} md={12} sm={12} xs={12} mt={3}>
                    <Button   variant='contained' color="success" onClick={handlerEditUser}>Thêm user</Button>
                    &ensp;<Button   variant='contained' color="success" onClick={handlerCloseModal}>Huỷ bỏ</Button>
                </Grid>
            </Grid>
          </Box>
        </Modal>
      </div>
    );
}