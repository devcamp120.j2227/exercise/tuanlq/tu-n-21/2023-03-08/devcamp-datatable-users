import Modal from '@mui/material/Modal';
import Typography from '@mui/material/Typography';
import Box from '@mui/material/Box';
import { Grid , Button } from '@mui/material';
import { closeModalDeleteUser, delteUserOnModal } from '../../actions/users.actions';
import { useDispatch, useSelector } from 'react-redux';
export default function ModalDeleteUser (){
    const {modalDeleteUser, userDetail} = useSelector ((reduxData) => reduxData.usersReducers);
    const dispatch = useDispatch();
    const style = {
        position: 'absolute',
        top: '50%',
        left: '50%',
        transform: 'translate(-50%, -50%)',
        width: 400,
        bgcolor: 'background.paper',
        boxShadow: 24,
        p: 4,
        borderRadius: "10px"
      };
      const handlerCloseModal = () => {
        dispatch(closeModalDeleteUser());
      };
      const handlerDeleteUser = () => {
        dispatch(delteUserOnModal(userDetail.id));
      };
      return (
        <div>
          <Modal
            keepMounted
            open = {modalDeleteUser}
            onClose = {handlerCloseModal}
            aria-labelledby="keep-mounted-modal-title"
            aria-describedby="keep-mounted-modal-description"
          >
            <Box sx={style}>
              <Grid container>
                  <Grid item lg={12} md={12} sm={12} xs={12} mt={3}>
                      <Typography variant="h4">Delete User</Typography>
                  </Grid>
                  <Grid item lg={12} md={12} sm={12} xs={12} mt={3}>
                      <Typography >Bạn chắc chắn muốn delete users này không?</Typography>
                  </Grid>
                  <Grid item lg={12} md={12} sm={12} xs={12} mt={3}>
                      <Button   variant='contained' color="error" onClick={handlerDeleteUser}>Delete user</Button>
                      &ensp;<Button   variant='contained' sx= {{backgroundColor:"grey"}} onClick={handlerCloseModal}>Huỷ bỏ</Button>
                  </Grid>
              </Grid>
            </Box>
          </Modal>
        </div>
      );
}