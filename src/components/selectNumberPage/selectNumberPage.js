import { useSelector, useDispatch } from "react-redux";
import { FormControl, Select, MenuItem  } from "@mui/material";
import { changeRowNumber } from "../../actions/users.actions";
export default function SelectNumberPage (){
    const dispatch = useDispatch();
    const {limit} = useSelector((reduxData) => reduxData.usersReducers);
    const handleChange = (event) => {
        dispatch(changeRowNumber(event.target.value));
    }
    return(
        <>
            <FormControl variant="standard" sx={{ m: 1, minWidth: 120 }}>
                <Select
                value={limit}
                onChange={handleChange}
                label="Rows"
                >
                <MenuItem value={0}>
                    <em>None</em>
                </MenuItem>
                <MenuItem value={5}>5</MenuItem>
                <MenuItem value={10}>10</MenuItem>
                <MenuItem value={25}>25</MenuItem>
                <MenuItem value={50}>50</MenuItem>
                </Select>
            </FormControl>
        </>
    )
}