import { CircularProgress, Container, Grid, Pagination, Paper, Table, TableBody, TableCell, TableContainer, TableHead, TableRow, Typography} from "@mui/material"
import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import Button from "@mui/material/Button";
import { fetchUsers, pageChangePagination } from "../actions/users.actions";
import ModalEditUser from "./modal/users.modalEditUser";
import { showModalEditUser, openDeleteUserModal } from "../actions/users.actions";
import SelectNumberPage from "./selectNumberPage/selectNumberPage";
import ButtonAddUser from "./button/buttonAddUser";
import ModalAddUser from "./modal/users.modalAddUser";
import ModalDeleteUser from "./modal/users.modalDeleteUser";
const Users = () => {
    const dispatch = useDispatch();

    const { users, pending, noPage, currentPage, limit } = useSelector((reduxData) => reduxData.usersReducers);

    useEffect(() => {
        // Gọi API để lấy dữ liệu
        dispatch(fetchUsers(currentPage, limit));
    }, [dispatch,limit, currentPage]);

    const onChangePagination = (event, value) => {
        dispatch(pageChangePagination(value));
    }
    const  handlerEditUser = (value) =>{
        console.log(value);
        dispatch(showModalEditUser(value));
    }
    const handlerDeleteUser = (value) => {
        console.log(value);
        dispatch(openDeleteUserModal(value));
    }
    return (
        <>
            <ModalEditUser></ModalEditUser>
            <ModalAddUser></ModalAddUser>
            <ModalDeleteUser></ModalDeleteUser>
            <Container>
                <Grid container mt={5}>
                    {  
                        pending ?
                            <Grid item lg={12} md={12} sm={12} xs={12} textAlign="center">
                                <CircularProgress />
                            </Grid>
                        :
                        <>  
                            <ButtonAddUser></ButtonAddUser>
                            <Grid item lg={12} md={12} sm={12} xs={12}>
                                <TableContainer component={Paper}>
                                    <Table sx={{ minWidth: 650 }} aria-label="simple table">
                                        <TableHead>
                                        <TableRow>
                                            <TableCell  align="center">ID</TableCell>
                                            <TableCell  align="center">Firstname</TableCell>
                                            <TableCell  align="center">Lastname</TableCell>
                                            <TableCell  align="center">country</TableCell>
                                            <TableCell  align="center">Subject</TableCell>
                                            <TableCell  align="center">Customer Type</TableCell>
                                            <TableCell  align="center">RegisterStatus</TableCell>
                                            <TableCell  align="center">Action</TableCell>
                                        </TableRow>
                                        </TableHead>
                                        <TableBody>
                                            { 
                                                users.map((element, index) => {
                                                    return (
                                                        <TableRow key={index}>
                                                            <TableCell  align="center" >{element.id}</TableCell>
                                                            <TableCell  align="center" >{element.firstname}</TableCell>
                                                            <TableCell  align="center">{element.lastname}</TableCell>
                                                            <TableCell  align="center">{element.country}</TableCell>
                                                            <TableCell  align="center">{element.subject}</TableCell>
                                                            <TableCell  align="center">{element.customerType}</TableCell>
                                                            <TableCell  align="center">{element.registerStatus}</TableCell>
                                                            <TableCell  align="center">
                                                                <Button onClick={() => handlerEditUser(element)} variant='contained' color="primary">Sửa</Button>
                                                                &ensp;
                                                                <Button onClick={() => handlerDeleteUser(element)} variant='contained' color="error">Xoá</Button>
                                                            </TableCell>
                                                        </TableRow>
                                                    ) 
                                                })
                                            }
                                        </TableBody>
                                    </Table>
                                </TableContainer>
                            </Grid>
                            <Grid item lg={6} md={6} sm={6} xs={6} mt={5}>
                                <Pagination count={noPage} page={currentPage} onChange={onChangePagination} />
                            </Grid>
                            <Grid item lg={2} md={2} sm={2} xs={2} mt ={5}>
                                <Typography>Row numbers:</Typography>
                            </Grid>
                            <Grid item lg={4} md={4} sm={4} xs={4} mt ={3}>
                                <SelectNumberPage></SelectNumberPage>
                            </Grid>
                        </>
                    }
                </Grid>
            </Container>
        </>
    )
}

export default Users;